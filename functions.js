function fruitProcessor(apples, oranges) {
  const juice = `Juice with ${apples} apples and ${oranges} oranges.`;
  return juice;
}
const appleJuice = fruitProcessor(5, 0);
console.log(appleJuice);
const appleOrangeJuice = fruitProcessor(2, 4);
console.log(appleOrangeJuice);

// Function declaration
function calcAge1(birthYeah) {
  return 2037 - birthYeah;
}
const age1 = calcAge1(1991);
// Function expression
const calcAge2 = function(birthYeah) {
  return 2037 - birthYeah;
};
const age2 = calcAge2(1991);
console.log(age1, age2);

// Arrow functions
const calcAge3 = birthYeah => 2037 - birthYeah;
const age3 = calcAge3(1991);
console.log(age3);
const yearsUntilRetirement = (birthYeah, firstName) => {
  const age = 2037 - birthYeah;
  const retirement = 65 - age;
  // return retirement;
  return `${firstName} retires in ${retirement} years`;
};
console.log(yearsUntilRetirement(1991, 'Ahmad'));
console.log(yearsUntilRetirement(1980, 'Bob'));

// Rest parameters

function sumAll(...args) {
  let sum = 0;

  for (let arg of args) sum += arg;

  return sum;
}

console.log(sumAll(1)); // 1
console.log(sumAll(1, 2)); // 3
console.log(sumAll(1, 2, 3)); // 6

// Arrow function

let sum = (a, b) => {
  let result = a + b;
  return result;
};

console.log(sum(1, 2)); // 3

// for in loop
const games = {
  first: 'football',
  second: 'baseball',
  third: 'handball',
  forth: 'surfing'
};
for (const a in games) {
  console.log(a);
}
