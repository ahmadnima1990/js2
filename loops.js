// for loop
for (let rep = 1; rep <= 30; rep++) {
  console.log(`Lifting weights repetition ${rep}`);
}

// Looping Arrays, Breaking and Continuing
const ahmad = [
  'ahmad',
  'gholamnia',
  2037 - 1991,
  'teacher',
  ['nima', 'kian', 'behnam'],
  true
];
const types = [];

for (let i = 0; i < ahmad.length; i++) {
  console.log(ahmad[i], typeof ahmad[i]);
  types.push(typeof ahmad[i]);
}

// Break   Continue

console.log('-----------------');

for (let i = 0; i < ahmad.length; i++) {
  if (typeof ahmad[i] !== 'string') continue;
  console.log(ahmad[i], typeof ahmad[i], 'ssss');
}
console.log('--- BREAK WITH NUMBER ---');
for (let i = 0; i < ahmad.length; i++) {
  if (typeof ahmad[i] === 'number') break;
  console.log(ahmad[i], typeof ahmad[i]);
}

// While loop
let rep = 1;
while (rep <= 10) {
  rep++;
}

let dice = Math.trunc(Math.random() * 6) + 1;
while (dice !== 6) {
  console.log(`You rolled a ${dice}`);
  dice = Math.trunc(Math.random() * 6) + 1;
  if (dice === 6) console.log('Loop is about to end...');
}

// For  of loop

const players = ['ahmad', 'nima', 'kian', 'omid', 'yaser', 'behnam'];
for (const [i, player] of players.entries()) {
  const [first] = player
    .toLowerCase()
    .trim()
    .split('_');
  const output = `${first}`;
  console.log(`${output}${'✅'.repeat(i + 1)}`);
}

// Do While

let i = 0;
do {
  alert(i);
  i++;
} while (i < 3);
