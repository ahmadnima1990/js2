let x;
let y;

x += y; //	x = x + y

x -= y; //	x = x - y

x *= y; //	x = x * y

x /= y; //	x = x / y

x %= y; //	x = x % y

x **= y; //	x = x ** y

x <<= y; //	x = x << y

x >>= y; //	x = x >> y

x >>>= y; //	x = x >>> y

x &= y; //	x = x & y

x ^= y; //	x = x ^ y

x |= y; //	x = x | y

// Destructuring Arrays
const arr = [2, 3, 4];
const a = arr[0];
const b = arr[1];
const c = arr[2];
const [xx, yy, zz] = arr;
console.log(xx, yy, zz);
console.log(arr);

// Destructuring Objects
const orderDelivery = {
  time: '22:30',
  address: 'tehran, shahriar',
  phone: '123456789',
  postCode: '8745'
};
const { time, address, phone, postCode } = orderDelivery;
console.log(time, address, phone, postCode);

// Short Circuiting (&& and ||)
console.log('---- OR ----');
console.log(3 || 'ahmad');
console.log('' || 'ahmad');
console.log(true || 0);
console.log(undefined || null);

console.log('---- AND ----');
console.log(0 && 'ahmad');
console.log(7 && 'ahmad');
console.log('Hello' && 23 && null && 'ahmad');

// typeof

let myFun = new Function('5 + 2');
let shape = 'round';
let size = 1;
let foo = ['Apple', 'Mango', 'Orange'];
let today = new Date();

console.log(typeof myFun); // returns "function"
console.log(typeof shape); // returns "string"
console.log(typeof size); // returns "number"
console.log(typeof foo); // returns "object"
console.log(typeof today); // returns "object"
console.log(typeof doesntExist); // returns "undefined"
